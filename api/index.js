urlAPI = "https://api.mercadolibre.com/sites/MLA/search?q=:'smart'";
var author = { name: "Juan Pablo", lastname: "Esteban S" };

const express = require("express");
const bodyParser = require("body-parser");
const fetch = require("node-fetch");
const cors = require("cors");

const app = express();
const port = process.env.PORT || 9000;

app.use(cors());
app.listen(port, () => console.log("Backend server live on " + port));

app.get("/", (req, res) => {
  console.log("req", req.originalUrl);
  fetch(urlAPI)
    .then((res) => res.json())
    .then((data) => {
      res.send({
        author,
        items: data.results,
        categories: !!data.filters ? data.filters : [],
        data,
      });
    })
    .catch((err) => {
      res.send(err);
    });
});

function serviceCall(options, JSON, next) {}
